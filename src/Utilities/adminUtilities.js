import { Tag } from "antd";

export const group = "GP01";

export const userColumns = [
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Mã loại người dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (ma) => {
      switch (ma) {
        case "GV":
          return <Tag color="red">Giáo viên</Tag>;
        case "HV":
          return <Tag color="blue">Học viên</Tag>;

        default:
          return <Tag color="gray">{ma}</Tag>;
      }
    },
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Số điện thoại",
    dataIndex: "soDt",
    key: "soDt",
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
  },
];

export const courseColumns = [
  {
    title: "Mã khóa học",
    dataIndex: "maKhoaHoc",
    key: "maKhoaHoc",
  },
  {
    title: "Tên khóa học",
    dataIndex: "tenKhoaHoc",
    key: "tenKhoaHoc",
  },

  {
    title: "Hình ảnh",
    dataIndex: "hinhAnh",
    key: "hinhAnh",
    render: (data) => {
      return (
        <div className="aspect-video w-[200px]">
          <img src={data} className="object-center w-full" alt="" />
        </div>
      );
    },
  },
  {
    title: "Ngày tạo",
    dataIndex: "ngayTao",
    key: "ngayTao",
  },
  {
    title: "Người tạo",
    dataIndex: "nguoiTao",
    key: "nguoiTao",
    render: (data) => {
      return (
        <>
          <p className="font-semibold">Tài khoản:</p>
          {data.taiKhoan}
          <p className="font-semibold">Họ tên:</p>
          {data.hoTen}
        </>
      );
    },
  },
  {
    title: "Danh mục",
    dataIndex: "danhMucKhoaHoc",
    key: "danhMucKhoaHoc",
    render: (data) => {
      return <>{data.tenDanhMucKhoaHoc}</>;
    },
  },

  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
  },
];

export const coursesColumns = [
  {
    title: "STT",
    key: "index",
    width: "60px",
    render: (id, record, index) => {
      ++index;
      return index;
    },
  },
  {
    title: "Tên khóa học",
    dataIndex: "tenKhoaHoc",
    key: "tenKhoaHoc",
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
  },
];

export const usersColumns = [
  {
    title: "STT",
    key: "index",
    width: "60px",
    render: (id, record, index) => {
      ++index;
      return index;
    },
  },
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Tên học viên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
  },
];

export const toKebabCase = (string) => {
  return string
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .toLowerCase()
    .replace(/\s+/g, "-");
};
