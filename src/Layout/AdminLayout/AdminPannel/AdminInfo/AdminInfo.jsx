import React from "react";
import { UilUserCircle } from "@iconscout/react-unicons";
import { useSelector } from "react-redux";

export default function AdminInfo() {
  let hoTen = useSelector((state) => state.userSlicer.userInfo?.hoTen);
  return (
    <div className="text-end text-xl w-full min-w-[200px] text-white">
      <div>
        <UilUserCircle
          size={34}
          className="inline-block mr-2 translate-y-[-2px]"
        />
        Chào, {hoTen}
        <button
          className="bg-white text-black px-3 py-1 ml-3 rounded text-lg border-2 hover:border-black duration-300"
          onClick={() => {
            window.location.href = "/";
          }}
        >
          Thoát
        </button>
      </div>
    </div>
  );
}
