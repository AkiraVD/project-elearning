import React, { useState } from "react";
import {
  UilListUl,
  UilListUiAlt,
  UilDesktop,
  UilUserCircle,
} from "@iconscout/react-unicons";
import { Menu } from "antd";
import { NavLink } from "react-router-dom";
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

export default function AdminPannel() {
  const [collapsed, setCollapsed] = useState(false);
  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };

  const items = [
    getItem("Quản lý người dùng", "sub1", <UilUserCircle />, [
      getItem(<NavLink to="/admin/main">Danh sách người dùng</NavLink>, "1"),
      getItem(<NavLink to="/Admin/AddUser">Thêm người dùng</NavLink>, "2"),
    ]),
    getItem("Quản lý khóa học", "sub2", <UilDesktop />, [
      getItem(<NavLink to="/Admin/Course">Danh sách khóa học</NavLink>, "5"),
      getItem(<NavLink to="/Admin/AddCourse">Thêm khóa học</NavLink>, "6"),
    ]),
  ];

  return (
    <div className="h-min-screen bg-sky-200">
      <div onClick={toggleCollapsed} className="cursor-pointer bg-sky-200 py-1">
        {collapsed ? (
          <UilListUl className="inline-block translate-y-[-2px] " />
        ) : (
          <UilListUiAlt className="inline-block translate-y-[-2px] " />
        )}
        <span> Menu</span>
      </div>
      <div>
        <Menu
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={["sub1"]}
          mode="inline"
          inlineCollapsed={collapsed}
          items={items}
          className="h-full text-start bg-sky-100 py-3"
        />
      </div>
    </div>
  );
}
