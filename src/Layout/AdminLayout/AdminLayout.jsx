import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import AdminHeader from "./AdminHeader";
import AdminPannel from "./AdminPannel/AdminPannel";

export default function AdminLayout({ children }) {
  let adminStatus = useSelector((state) => state.adminSlicer.isAdminLogin);
  let navigate = useNavigate();
  useEffect(() => {
    if (!adminStatus) {
      navigate("/admin/login");
    }
  }, [adminStatus]);

  return (
    <div className="min-h-screen flex flex-col">
      <AdminHeader />
      <div className="flex grow">
        <AdminPannel />
        {children}
      </div>
    </div>
  );
}
