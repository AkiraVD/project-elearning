import React from "react";
import Logo from "../../Components/Logo/Logo";
import AdminInfo from "./AdminPannel/AdminInfo/AdminInfo";

export default function AdminHeader() {
  return (
    <div className="flex">
      <div className="bg-sky-500 p-3 flex justify-between items-center grow">
        <Logo />
        <AdminInfo />
      </div>
    </div>
  );
}
