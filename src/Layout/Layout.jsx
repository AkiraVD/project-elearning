import React from "react";
import Footer from "../Components/Footer/Footer";
import Header from "../Components/Header/Header";

export default function Layout({ children }) {
  return (
    <div className="flex flex-col justify-between min-h-[100vh]">
      <Header />
      {children}
      <Footer />
    </div>
  );
}
