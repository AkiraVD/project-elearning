import { group } from "../Utilities/adminUtilities";
import { https } from "./configURL";

export const learningService = {
  getDanhSachKhoaHoc: () => {
    return https.get(`/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=${group}`);
  },
  getDanhMucKhoaHoc: () => {
    return https.get(`/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc`);
  },
  getKhoaHocTheoDanhMuc: (params) => {
    return https.get(
      `/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${params}&MaNhom=${group}`
    );
  },
  getChiTietKhoaHoc: (params) => {
    return https.get(
      `/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${params}`
    );
  },
  getTimKiemKhoaHoc: (params) => {
    return https.get(
      `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?tenKhoaHoc=${params}&MaNhom=${group}`
    );
  },
  postDangkyKhoaHoc: (data) => {
    return https.post(`/api/QuanLyKhoaHoc/DangKyKhoaHoc`, data);
  },
  postHuyGhiDanh: (data) => {
    return https.post(`/api/QuanLyKhoaHoc/HuyGhiDanh`, data);
  },
  postGhiDanh: (data) => {
    return https.post(`/api/QuanLyKhoaHoc/GhiDanhKhoaHoc`, data);
  },
  postUploadHinhAnhKhoaHoc: (data) => {
    return https.post(`/api/QuanLyKhoaHoc/UploadHinhAnhKhoaHoc`, data);
  },
  postCapNhatHinhAnhKhoaHoc: (data) => {
    return https.post(`/api/QuanLyKhoaHoc/CapNhatKhoaHocUpload`, data)
  }
};
