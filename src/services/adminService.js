import { group } from "../Utilities/adminUtilities";
import { https } from "./configURL";

export const adminService = {
  // API Quản lý user
  getDanhSachNguoiDung: () => {
    return https.get(
      `/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${group}`
    );
  },
  getTimKiemNguoiDung: (taiKhoan) => {
    return https.get(
      `/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=${group}&tuKhoa=${taiKhoan}`
    );
  },
  deleteNguoiDung: (taiKhoan) => {
    return https.delete(
      `/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`
    );
  },
  putCapNhatNguoiDung: (data) => {
    return https.put(`/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`, {
      ...data,
      maNhom: group,
    });
  },
  postDanhSachKhoaHocChuaGhiDanh: (taiKhoan) => {
    return https.post(
      `/api/QuanLyNguoiDung/LayDanhSachKhoaHocChuaGhiDanh?TaiKhoan=${taiKhoan}`
    );
  },
  postDanhSachKhoaHocChoXetDuyet: (taiKhoan) => {
    return https.post(`/api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet`, {
      taiKhoan,
    });
  },
  postDanhSachKhoaHocDaXetDuyet: (taiKhoan) => {
    return https.post(`/api/QuanLyNguoiDung/LayDanhSachKhoaHocDaXetDuyet`, {
      taiKhoan,
    });
  },
  postGhiDanhKhoaHoc: (taiKhoan, maKhoaHoc) => {
    return https.post(`/api/QuanLyKhoaHoc/GhiDanhKhoaHoc`, {
      taiKhoan,
      maKhoaHoc,
    });
  },
  postXacThucKhoaHoc: (taiKhoan, maKhoaHoc) => {
    return https.post(`/api/QuanLyKhoaHoc/DangKyKhoaHoc`, {
      taiKhoan,
      maKhoaHoc,
    });
  },
  postHuyGhiDanh: (taiKhoan, maKhoaHoc) => {
    return https.post(`/api/QuanLyKhoaHoc/HuyGhiDanh`, {
      taiKhoan,
      maKhoaHoc,
    });
  },
  postThemNguoiDung: (data) => {
    return https.post(`/api/QuanLyNguoiDung/ThemNguoiDung`, data);
  },
  // API Quản lý khóa học
  getTimKiemKhoaHoc: (keyword) => {
    return https.get(
      `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?tenKhoaHoc=${keyword}&MaNhom=${group}`
    );
  },
  postThemKhoaHoc: (data) => {
    return https.post(`/api/QuanLyKhoaHoc/ThemKhoaHoc`, data);
  },
  postUploadHinhAnhKhoaHoc: (data) => {
    return https.post(`/api/QuanLyKhoaHoc/UploadHinhAnhKhoaHoc`, data);
  },
  deleteKhoaHoc: (maKhoaHoc) => {
    return https.delete(`/api/QuanLyKhoaHoc/XoaKhoaHoc?MaKhoaHoc=${maKhoaHoc}`);
  },
  putCapNhatKhoaHoc: (data) => {
    return https.put(`/api/QuanLyKhoaHoc/CapNhatKhoaHoc`, data);
  },
  postDanhSachNguoiDungChuaGhiDanh: (maKhoaHoc) => {
    return https.post(`/api/QuanLyNguoiDung/LayDanhSachNguoiDungChuaGhiDanh`, {
      maKhoaHoc: maKhoaHoc,
    });
  },
  postDanhSachHocVienChoXetDuyet: (maKhoaHoc) => {
    return https.post(`/api/QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet`, {
      maKhoaHoc,
    });
  },
  postDanhSachHocVienKhoaHoc: (maKhoaHoc) => {
    return https.post(`/api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc`, {
      maKhoaHoc,
    });
  },
};
