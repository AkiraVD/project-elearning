import { https } from "./configURL";

export const userService = {
  postDangNhap: (userData) => {
    return https.post(`/api/QuanLyNguoiDung/DangNhap`, userData);
  },
  postDangky: (userData) => {
    return https.post(`/api/QuanLyNguoiDung/DangKy`, userData);
  },
  putCapNhatThongTin: (userData) => {
    return https.put(`/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`, userData);
  },
  postThongTinNguoiDung: () => {
    return https.post(`/api/QuanLyNguoiDung/ThongTinNguoiDung`);
  },
  postThongTintaiKhoan: (taiKhoan) => {
    return https.post(`/api/QuanLyNguoiDung/ThongTinTaiKhoan`, taiKhoan);
  },
  postLayDanhSachKhoaHocChuaGhiDanh: (taiKhoan) => {
    return https.post(
      `/api/QuanLyNguoiDung/LayDanhSachKhoaHocChuaGhiDanh`,
      taiKhoan
    );
  },
  postLayDanhSachKhoaHocChoXetDuyet: (taiKhoan) => {
    return https.post(`/api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet`, {
      taiKhoan,
    });
  },
  LayDanhSachKhoaHocDaXetDuyet: (taiKhoan) => {
    return https.post(`/api/QuanLyNguoiDung/LayDanhSachKhoaHocDaXetDuyet`, {
      taiKhoan,
    });
  },
};
