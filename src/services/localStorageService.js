export const LOGIN_INFO = "LOGIN_INFO";

export const userLocalService = {
  set: (userData) => {
    let userJSON = JSON.stringify(userData);
    localStorage.setItem(LOGIN_INFO, userJSON);
  },
  get: () => {
    let userJSON = localStorage.getItem(LOGIN_INFO);
    if (userJSON !== null) {
      return JSON.parse(userJSON);
    } else {
      return null;
    }
  },
  remove: () => {
    localStorage.removeItem(LOGIN_INFO);
  },
};
