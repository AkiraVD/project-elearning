import axios from "axios";
import { store } from "../app/store";
import {
  setLoadingOff,
  setLoadingOn,
} from "../redux-toolkit/slice/loadingSlicer";
import { userLocalService } from "./localStorageService";

export const BASE_URL = "https://elearningnew.cybersoft.edu.vn";
export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNSIsIkhldEhhblN0cmluZyI6IjI4LzA1LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTIzMjAwMDAwMCIsIm5iZiI6MTY2MjMxMDgwMCwiZXhwIjoxNjg1Mzc5NjAwfQ.FtGbsXl4qyqTRfJrunro0mQ7b-tNs8EWbhb7JDTzloE";
export const createConfig = () => {
  return {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: "Bearer " + userLocalService.get()?.accessToken,
  };
};

export const https = axios.create({
  baseURL: BASE_URL,
  headers: createConfig(),
});

export const adminHttps = axios.create({
  baseURL: BASE_URL,
  headers: createConfig(),
});

https.interceptors.request.use(
  function (config) {
    store.dispatch(setLoadingOn());
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

https.interceptors.response.use(
  function (response) {
    store.dispatch(setLoadingOff());
    return response;
  },
  function (error) {
    store.dispatch(setLoadingOff());
    return Promise.reject(error);
  }
);
