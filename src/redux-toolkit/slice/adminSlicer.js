import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isAdminLogin: false,
};

export const adminSlicer = createSlice({
  name: "adminSlicer",
  initialState,
  reducers: {
    setAdminLoginStatus: (state, action) => {
      state.isAdminLogin = true;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setAdminLoginStatus } = adminSlicer.actions;

export default adminSlicer.reducer;
