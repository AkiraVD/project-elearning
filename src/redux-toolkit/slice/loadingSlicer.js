import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
};

export const loadingSlicer = createSlice({
  name: "loadingSlicer",
  initialState,
  reducers: {
    setLoadingOn: (state, action) => {
      state.isLoading = true;
    },
    setLoadingOff: (state, action) => {
      state.isLoading = false;
    },
  },
});

export const { setLoadingOn, setLoadingOff } = loadingSlicer.actions;

export default loadingSlicer.reducer;
