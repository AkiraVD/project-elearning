import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  danhMucKhoaHoc: [],
  tenKhoaHoc: "",
};

export const khoaHocSlicer = createSlice({
  name: "khoaHocSlicer",
  initialState,
  reducers: {
    setDanhMucKhoaHoc: (state, action) => {
      state.danhMucKhoaHoc = action.payload;
    },
    setTenKhoaHoc: (state, action) => {
      state.tenKhoaHoc = action.payload;
    },
  },
});

export const {
  setDanhMucKhoaHoc,
  setTenKhoaHoc,
  setKhoaHocGhiDanh,
  removeKhoaHoc,
} = khoaHocSlicer.actions;

export default khoaHocSlicer.reducer;
