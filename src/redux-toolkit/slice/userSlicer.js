import { createSlice } from "@reduxjs/toolkit";
import { userLocalService } from "../../services/localStorageService";

const initialState = {
  userInfo: userLocalService.get(),
};

export const userSlicer = createSlice({
  name: "userSlicer",
  initialState,
  reducers: {
    setUserInfo: (state, action) => {
      state.userInfo = action.payload;
    },
  },
});

export const { setUserInfo } = userSlicer.actions;

export default userSlicer.reducer;
