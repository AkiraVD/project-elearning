import { configureStore } from "@reduxjs/toolkit";
import adminSlicer from "../redux-toolkit/slice/adminSlicer";
import khoaHocSlicer from "../redux-toolkit/slice/khoaHocSlicer";
import loadingSlicer from "../redux-toolkit/slice/loadingSlicer";
import userSlicer from "../redux-toolkit/slice/userSlicer";
export const store = configureStore({
  reducer: { loadingSlicer, khoaHocSlicer, userSlicer, adminSlicer },
});
