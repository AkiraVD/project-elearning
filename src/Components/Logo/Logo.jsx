import React from "react";
import { NavLink } from "react-router-dom";
import logo from "../../assets/logo.webp";

export default function Logo() {
  return (
    <NavLink to="/">
      <img src={logo} alt="Logo" className="max-w min-w-[200px]" />
    </NavLink>
  );
}
