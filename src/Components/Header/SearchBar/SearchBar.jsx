import Search from "antd/es/input/Search";
import React from "react";
import { useNavigate } from "react-router-dom";
import { UilSearch } from "@iconscout/react-unicons";

export default function SearchBar({ onClose }) {
  const navigate = useNavigate();

  const onSearch = (value) => {
    onClose();
    if (value == "") {
      return;
    } else {
      navigate(`/TimKiemKhoaHoc/${value}`);
    }
  };
  return (
    <div className="w-full">
      <Search
        placeholder="Tìm kiếm"
        onSearch={onSearch}
        enterButton={<UilSearch />}
        style={{
          border: "1px solid #60d0dc",
          borderRadius: "10px",
          overflow: "hidden",
        }}
      />
    </div>
  );
}
