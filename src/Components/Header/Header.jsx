import React, { useState } from "react";
import Logo from "../Logo/Logo";
import DanhMuc from "./DanhMuc/DanhMuc";
import SearchBar from "./SearchBar/SearchBar";
import UserPanel from "./UserPanel/UserPanel";
import { UisBars } from "@iconscout/react-unicons-solid";
import { Drawer } from "antd";
import { UilTimesCircle } from "@iconscout/react-unicons";

export default function Header() {
  const [open, setOpen] = useState(false);
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  return (
    <>
      <div className="bg-sky-500 p-3 sm:flex justify-between gap-5 items-center hidden">
        <Logo />
        <DanhMuc />
        <SearchBar onClose={onClose} />
        <UserPanel />
      </div>
      <div className="bg-sky-500 p-3 flex justify-between gap-5 items-center sm:hidden">
        <div className="w-2/3">
          <Logo />
        </div>
        <button
          onClick={showDrawer}
          className="rounded bg-sky-200 hover:bg-white px-2 py-1"
        >
          <UisBars className="inline-block lg:mr-1 translate-y-[-1px]" />
        </button>
      </div>
      <Drawer
        headerStyle={{ background: "#e0f2fe", padding: "5px 0" }}
        bodyStyle={{ padding: "5px" }}
        style={{ background: "#0EA5E9" }}
        size="large"
        title={
          <>
            <UserPanel onClose={onClose} />
          </>
        }
        footerStyle={{ textAlign: "center" }}
        footer={
          <UilTimesCircle
            className="inline-block text-sky-300"
            onClick={onClose}
          />
        }
        closable={false}
        placement="top"
        onClose={onClose}
        open={open}
      >
        <SearchBar onClose={onClose} />
        <DanhMuc onClose={onClose} />
      </Drawer>
    </>
  );
}
