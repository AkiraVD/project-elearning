import { Dropdown, Menu } from "antd";
import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { learningService } from "../../../services/learningService";
import { UisBars } from "@iconscout/react-unicons-solid";
import { store } from "../../../app/store";
import { setDanhMucKhoaHoc } from "../../../redux-toolkit/slice/khoaHocSlicer";

export default function DanhMuc({ onClose }) {
  const [arrDanhMuc, setArrDanhMuc] = useState([]);
  useEffect(() => {
    learningService
      .getDanhMucKhoaHoc()
      .then((res) => {
        let danhMuc = res.data.map((item, index) => {
          return {
            key: index + 1,
            label: (
              <NavLink
                to={`/DanhMucKhoaHoc/${item.maDanhMuc}`}
                onClick={onClose}
              >
                {item.tenDanhMuc}
              </NavLink>
            ),
          };
        });
        setArrDanhMuc(danhMuc);
        store.dispatch(setDanhMucKhoaHoc(res.data));
      })
      .catch((err) => {});
  }, []);

  const rootSubmenuKeys = ["sub1", "sub2", "sub4"];
  const [openKeys, setOpenKeys] = useState(["sub1"]);
  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };
  return (
    <>
      <div>
        <Dropdown
          menu={{ items: arrDanhMuc }}
          className="w-max hidden lg:inline-block"
          trigger={["click", "hover"]}
        >
          <button className="rounded bg-sky-200 hover:bg-white px-3 py-1">
            <UisBars className="inline-block lg:mr-1 translate-y-[-1px]" />
            <span className="">Danh sách khóa học</span>
          </button>
        </Dropdown>
        <Dropdown
          menu={{
            items: [
              {
                key: 0,
                type: "group",
                label: <span className="">Danh sách khóa học</span>,
              },
              {
                type: "divider",
              },
              ...arrDanhMuc,
            ],
          }}
          trigger={["click", "hover"]}
          className="w-max hidden sm:inline-block lg:hidden"
        >
          <button className="rounded bg-sky-200 hover:bg-white px-3 py-1">
            <UisBars className=" lg:mr-1 translate-y-[-1px]" />
          </button>
        </Dropdown>
      </div>
      <Menu
        className="sm:hidden bg-sky-500 text-sky-100 font-semibold text-xl"
        mode="inline"
        openKeys={openKeys}
        onOpenChange={onOpenChange}
        style={{ width: "100%" }}
        items={[
          {
            label: (
              <p className="target:text-white text-white font-bold border-b-white border-b-2">
                Danh sách khóa học
              </p>
            ),
            key: "sub1",
            children: arrDanhMuc,
          },
        ]}
      />
    </>
  );
}
