import React, { useState } from "react";
import { Dropdown, Menu, message } from "antd";
import { UilUserCircle } from "@iconscout/react-unicons";
import { userLocalService } from "../../../../services/localStorageService";
import { NavLink } from "react-router-dom";

export default function LogingIn({ onClose, userInfo }) {
  const handleDangXuat = () => {
    userLocalService.remove();
    message.success("Đăng xuất thành công");
    setTimeout(() => {
      window.location.href = "/";
    }, 1000);
  };
  const items = [
    {
      label: (
        <NavLink to="/ThongTinTaiKhoan" onClick={onClose}>
          Thông tin người dùng
        </NavLink>
      ),
      key: "0",
    },
    {
      label: (
        <NavLink to="/Admin/login" onClick={onClose}>
          Trang quản lý
        </NavLink>
      ),
      key: "1",
      disabled: userInfo.maLoaiNguoiDung == "GV" ? false : true,
    },
    {
      type: "divider",
    },
    {
      label: (
        <p className="text-red-500" onClick={handleDangXuat}>
          Đăng xuất
        </p>
      ),
      key: "3",
    },
  ];

  const [openKeys, setOpenKeys] = useState(["sub2"]);
  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };
  const rootSubmenuKeys = ["sub1", "sub2", "sub4"];

  return (
    <>
      <div className="translate-y-[1px] min-w-max hidden sm:inline-block">
        <Dropdown
          menu={{
            items,
          }}
          trigger={["click", "hover"]}
        >
          <div
            className="text-xl cursor-pointer w-full min-w-[200px] text-white"
            onClick={(e) => e.preventDefault()}
          >
            <div>
              <UilUserCircle
                size={34}
                className="inline-block mr-2 translate-y-[-2px]"
              />
              Xin chào, {userInfo.hoTen}
            </div>
          </div>
        </Dropdown>
      </div>
      <Menu
        className="sm:hidden bg-sky-100 rounded-md"
        mode="inline"
        openKeys={openKeys}
        onOpenChange={onOpenChange}
        style={{ width: "100%" }}
        // items={items}
        items={[
          {
            label: (
              <div>
                <UilUserCircle
                  size={34}
                  className="inline-block mr-2 translate-y-[-2px]"
                />
                <span className="text-xl hover:text-black">
                  Xin chào, {userInfo.hoTen}
                </span>
              </div>
            ),
            key: "sub2",
            children: items,
          },
        ]}
      />
    </>
  );
}
