import React from "react";
import { useSelector } from "react-redux";
import LogingIn from "./LogingIn/LogingIn";
import NotLogin from "./NotLogin/NotLogin";

export default function UserPanel({ onClose }) {
  let userInfo = useSelector((state) => state.userSlicer.userInfo);
  return (
    <>
      {userInfo == null ? (
        <NotLogin onClose={onClose} />
      ) : (
        <LogingIn onClose={onClose} userInfo={userInfo} />
      )}
    </>
  );
}
