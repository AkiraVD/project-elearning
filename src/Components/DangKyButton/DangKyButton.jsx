import { message } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { learningService } from "../../services/learningService";

export default function DangKyButton({ maKhoaHoc }) {
  let navigate = useNavigate();
  let userInfo = useSelector((state) => state.userSlicer.userInfo);
  const handleDangKyKhoaHoc = () => {
    if (!userInfo) {
      message.error("Mời bạn đăng nhập trước khi đăng ký khóa học");
      setTimeout(() => {
        navigate("/DangNhap");
      }, 1000);
    } else {
      learningService
        .postDangkyKhoaHoc({ maKhoaHoc, taiKhoan: userInfo.taiKhoan })
        .then((res) => {
          message.success("Đăng ký thành công khóa học " + maKhoaHoc);
        })
        .catch((err) => {
          message.error(err.response.data);
        });
    }
  };

  return (
    <button
      onClick={handleDangKyKhoaHoc}
      className=" bg-green-500 px-3 py-1 rounded text-white font-semibold hover:opacity-60 active:opacity-30 duration-300"
    >
      Đăng ký
    </button>
  );
}
