import { Button, Input } from "antd";
import React, { useState } from "react";
import { UilMapMarker } from "@iconscout/react-unicons";
import { UilPhone } from "@iconscout/react-unicons";
import Logo from "../../Logo/Logo";

export default function FooterLeft() {
  const [email, setEmail] = useState("");
  return (
    <div className="text-left sm:px-5 mb-5">
      <div className="">
        <div className="w-[95%]">
          <Logo />
        </div>
        <h3 className="mt-2">
          Hệ thống đào tạo lập trình chuyên sâu theo dự án thực tế
        </h3>
      </div>
      <div className="my-5">
        <h3 className="uppercase font-semibold text-xl">
          Nhận tin sự kiện & khuyến mãi
        </h3>
        <p>
          Trung tâm sẽ gửi các khóa học trực tuyến & các chương trình Live hoàn
          toàn MIỄN PHÍ và các chương trình khuyến mãi hấp dẫn đến các bạn.
        </p>
        <div className="mt-3">
          <Input
            value={email}
            placeholder="your.address@email.com"
            onChange={(e) => setEmail(e.target.value)}
            className="inline-block lg:w-[70%] lg:mr-2"
          />
          <Button
            type="success"
            className="bg-emerald-500 text-white font-semibold duration-150 hover:bg-emerald-400 active:bg-emerald-700 mt-3"
          >
            Đăng ký
          </Button>
        </div>
      </div>
      <div>
        <p>
          <UilMapMarker className="inline-block" size="20" /> Cơ sở 1: Võ Văn
          Tần - Quận 3
        </p>
        <p>
          <UilMapMarker className="inline-block" size="20" /> Cơ sở 2: Sư Vạn
          Hạnh - Quận 10
        </p>
        <p>
          <UilMapMarker className="inline-block" size="20" /> Cơ sở 3: Ung Văn
          Khiêm - Bình Thạnh
        </p>
        <p>
          <UilPhone className="inline-block" size="20" /> 096 206 4249
        </p>
      </div>
    </div>
  );
}
