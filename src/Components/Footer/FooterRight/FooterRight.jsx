import React from "react";
import { NavLink } from "react-router-dom";
import footer_banner from "../../../assets/footer-banner.bmp";

export default function FooterRight() {
  return (
    <div className=" sm:px-5 mt-5">
      <div className=" w-full drop-shadow rounded overflow-hidden">
        <img
          src={footer_banner}
          alt=""
          className="w-full object-cover object-center aspect-[2/1]"
        />
      </div>
      <div className="text-justify mt-4">
        <NavLink>Anh ngữ giao tiếp</NavLink>
        {" - "}
        <NavLink>Khởi động Anh ngữ giao tiếp</NavLink>
        {" - "}
        <NavLink>Lấy đà Anh ngữ giao tiếp</NavLink>
        {" - "}
        <NavLink>Bật nhảy Anh ngữ giao tiếp</NavLink>
        {" - "}
        <NavLink>Bay trên không Anh ngữ giao tiếp</NavLink>
        {" - "}
        <NavLink>Tiếp đất</NavLink>
      </div>
    </div>
  );
}
