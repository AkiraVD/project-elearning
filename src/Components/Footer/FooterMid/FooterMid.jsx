import React from "react";
import { Button, Form, Input, message } from "antd";
import { NavLink } from "react-router-dom";

export default function FooterMid() {
  const [form] = Form.useForm();
  const handleFinish = (values) => {
    message.success("Cám ơn bạn đã đăng ký");
    form.resetFields();
  };
  return (
    <div id="dangky" className="sm:px-5 text-left">
      <div>
        <h3 className="font-semibold text-xl mb-3">ĐĂNG KÝ TƯ VẤN</h3>
        <Form
          form={form}
          layout="vertical"
          autoComplete="off"
          onFinish={handleFinish}
        >
          <Form.Item name="name">
            <Input placeholder="Họ và tên *" />
          </Form.Item>
          <Form.Item name="email">
            <Input placeholder="Email liên hệ *" />
          </Form.Item>
          <Form.Item name="phone">
            <Input placeholder="Điện thoại liên hệ" />
          </Form.Item>
          <Form.Item>
            <Button
              type="success"
              htmlType="submit"
              className="bg-emerald-500 text-white font-semibold duration-150 hover:bg-emerald-400 active:bg-emerald-700"
            >
              Đăng ký tư vấn
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div>
        <NavLink className="mr-3">Lập trình Front End</NavLink>
        <NavLink className="mr-3">Lập trình React JS</NavLink>
        <NavLink className="mr-3">Lập trình React Angular</NavLink>
        <NavLink className="mr-3">Lập trình tư duy</NavLink>
        <NavLink className="mr-3">Lập trình NodeJS</NavLink>
        <NavLink className="mr-3">Lập trình Backend</NavLink>
        <NavLink className="mr-3">Lập trình Java Web</NavLink>
        <NavLink className="mr-3">Lập trình Java Spring</NavLink>
      </div>
    </div>
  );
}
