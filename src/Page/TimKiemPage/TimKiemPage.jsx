import { Pagination } from "antd";
import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { store } from "../../app/store";
import VideoItem from "../../Components/VideoItem/VideoItem";
import { setLoadingOff } from "../../redux-toolkit/slice/loadingSlicer";
import { learningService } from "../../services/learningService";

export default function TimKiemPage() {
  let params = useParams();

  //Pagnation
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setpageSize] = useState(8);
  const [content, setContent] = useState([]);
  const [total, setTotal] = useState(0);
  const [arrKhoaHoc, setArrKhoaHoc] = useState([]);

  useEffect(() => {
    learningService
      .getTimKiemKhoaHoc(params.tuKhoa)
      .then((res) => {
        setArrKhoaHoc(res.data);
        setContent(res.data.slice(0, 8));
        setTotal(res.data.length);
      })
      .catch((err) => {
        store.dispatch(setLoadingOff());
        setTotal(0);
        setContent([]);
      });
  }, [params]);

  useEffect(() => {
    setContent(
      arrKhoaHoc.slice((currentPage - 1) * pageSize, pageSize * currentPage)
    );
  }, [pageSize, currentPage]);

  const onChange = (page) => {
    setCurrentPage(page);
  };

  const handleRenderKhoaHoc = () => {
    return content?.map((item) => {
      return <VideoItem key={nanoid()} item={item} />;
    });
  };

  return (
    <div className="flex flex-col items-start grow">
      <div className="w-full bg-gradient-to-r from-green-700 to-blue-400">
        <div className="container mx-auto text-start py-5 px-3">
          <p className="font-bold text-xl sm:text-2xl md:text-3xl lg:text-4xl text-white">
            Tìm kiếm
          </p>
        </div>
      </div>
      <div className="container mx-auto mt-4 px-3">
        <h2 className="text-start text-2xl pb-5 font-semibold border-b border-gray-200">
          Tìm thấy <span>{total}</span> khóa học "<span>{params.tuKhoa}</span>"
        </h2>
        <div className="flex flex-col-reverse md:flex-col mb-5">
          <div className="sm:mt-5">
            <Pagination
              defaultCurrent={1}
              current={currentPage}
              onChange={onChange}
              pageSize={pageSize}
              total={total}
            />
          </div>
          <div className="grid px-5 md:grid-cols-2 xl:grid-cols-4 gap-5 my-5">
            {handleRenderKhoaHoc()}
          </div>
        </div>
      </div>
    </div>
  );
}
