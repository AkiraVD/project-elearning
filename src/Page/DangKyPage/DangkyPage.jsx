import { Button, Form, Input, message } from "antd";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { userService } from "../../services/userService";
import { group } from "../../Utilities/adminUtilities";
import { rules } from "../../Utilities/validatedRules";

export default function DangkyPage() {
  let navigate = useNavigate();
  const [form] = Form.useForm();
  const [formData, setFormData] = useState({});
  const [showPassword, setShowPassword] = useState(false);

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    form.validateFields((error, values) => {
      if (!error) {
        setFormData(values);
      }
    });
  };

  const handleFinish = (values) => {
    let userData = { ...values, maNhom: group };
    userService
      .postDangky(userData)
      .then((res) => {
        message.success("Tạo tài khoản thành công");
        setTimeout(() => {
          message.success("Chuyển sang trang đăng nhập");
        }, 500);
        setTimeout(() => {
          navigate("/DangNhap");
        }, 1500);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  return (
    <div className="container mx-auto h-full max-w-[600px]">
      <Form form={form} onSubmit={handleSubmit} onFinish={handleFinish}>
        <h2 className="text-3xl font-bold text-sky-500 my-5">Đăng ký</h2>

        <Form.Item label="Tài khoản" name="taiKhoan" rules={rules.account}>
          <Input placeholder="Tên tài khoản" />
        </Form.Item>
        <Form.Item label="Mật khẩu" name="matKhau" rules={rules.password}>
          <Input.Password
            placeholder="8 ký tự, bao gồm cả chữ và số, có ít nhất 1 chữ in hoa, 1 chữ thường"
            eye
            visibilityToggle
            onClick={handleShowPassword}
            visible={showPassword}
          />
        </Form.Item>
        <Form.Item
          label="Nhập lại mật khẩu"
          name="matKhauXacNhan"
          dependencies={["matKhau"]}
          rules={rules.repassword}
          className="mb-4"
        >
          <Input.Password
            placeholder="Nhập lại mật khẩu"
            eye
            visibilityToggle
            onClick={handleShowPassword}
            visible={showPassword}
          />
        </Form.Item>
        <Form.Item label="Họ và tên" name="hoTen" rules={rules.name}>
          <Input placeholder="VD: Nguyễn Văn A" />
        </Form.Item>

        <Form.Item label="Địa chỉ Email" name="email" rules={rules.email}>
          <Input type="email" placeholder="abc123@mail.com" />
        </Form.Item>

        <Form.Item label="Số điện thoại" name="soDt" rules={rules.phone}>
          <Input placeholder="VD: 0901234567" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" className="bg-blue-500" htmlType="submit">
            Đăng ký
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
