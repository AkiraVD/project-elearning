import { message } from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { learningService } from "../../services/learningService";
import coding_bg from "../../assets/coding-bg.jpg";
import { UisStar } from "@iconscout/react-unicons-solid";
import { UisStarHalfAlt } from "@iconscout/react-unicons-solid";
import DangKyButton from "../../Components/DangKyButton/DangKyButton";

export default function ChiTietPage() {
  let params = useParams();
  let navigate = useNavigate();
  const [thongTin, setThongTin] = useState({});
  useEffect(() => {
    learningService
      .getChiTietKhoaHoc(params.maKhoaHoc)
      .then((res) => {
        setThongTin(res.data);
      })
      .catch((err) => {
        message.error(err.response.data);
        setTimeout(() => {
          navigate("/");
        }, 2000);
      });
  }, []);

  const backgounf_style = {
    backgroundImage: `url(${coding_bg})`,
    backgroundSize: "cover",
  };

  const handleRender = () => {
    return (
      <>
        <div style={backgounf_style} className="">
          <div className="container mx-auto flex flex-col-reverse sm:flex-row p-10 ">
            <div className="sm:w-1/2 p-5 text-start">
              <h2 className="text-white text-4xl font-semibold mb-3">
                {thongTin?.danhMucKhoaHoc?.tenDanhMucKhoaHoc} <br />
                {thongTin?.tenKhoaHoc}
              </h2>
              <h2 className="text-white text-xl">Đánh giá khóa học</h2>
              <div className="mb-5">
                <UisStar size="20" className="text-yellow-400 inline-block" />
                <UisStar size="20" className="text-yellow-400 inline-block" />
                <UisStar size="20" className="text-yellow-400 inline-block" />
                <UisStar size="20" className="text-yellow-400 inline-block" />
                <UisStarHalfAlt
                  size="20"
                  className="text-yellow-400 inline-block"
                />
              </div>
              <DangKyButton maKhoaHoc={params.maKhoaHoc} />
            </div>
            <div className="sm:w-1/2 aspect-video flex flex-col justify-center bg-black overflow-hidden text-white font-bold text-center text-4xl">
              <img
                className="object-center w-full"
                src={thongTin?.hinhAnh}
                alt={thongTin?.tenKhoaHoc}
              />
            </div>
          </div>
        </div>
        <div className="container mx-auto py-5 px-10 text-start">
          <h2 className="text-2xl pb-5 font-semibold border-b border-gray-200 mb-3">
            Thông tin khóa học
          </h2>
          <h3 className="indent-8">
            <span className="font-semibold">Số lượng học viên: </span>
            {thongTin?.soLuongHocVien}
          </h3>
          <h3 className="indent-8">
            <span className="font-semibold">Mô tả: </span> <br />
          </h3>
          <p className="text-justify indent-8">{thongTin?.moTa}</p>

          <p className="text-justify indent-8">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda
            tempore minima, libero ipsa similique molestiae rem? Qui voluptate
            quam velit dicta deserunt, suscipit fugit architecto cumque,
            expedita repudiandae illum. Amet. Commodi molestiae modi,
            consequuntur id nostrum consequatur voluptatibus dolor in ea,
            pariatur libero dolores quasi similique optio explicabo officia!
            Autem harum, quisquam voluptatum eligendi qui distinctio quibusdam
            minima architecto non? Laborum possimus ullam ipsam reiciendis alias
            aperiam quia accusamus voluptatibus distinctio, consequuntur ratione
            dolor quidem deleniti atque nulla nisi eveniet, modi, unde soluta
            fugiat.
          </p>
        </div>
      </>
    );
  };

  return <div className="grow">{handleRender()}</div>;
}
