import { message, Pagination } from "antd";
import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { store } from "../../app/store";
import VideoItem from "../../Components/VideoItem/VideoItem";
import { setTenKhoaHoc } from "../../redux-toolkit/slice/khoaHocSlicer";
import { learningService } from "../../services/learningService";

export default function DanhMucKhoaHocPage() {
  let params = useParams();
  let navigate = useNavigate();
  const { tenKhoaHoc, danhMucKhoaHoc } = useSelector(
    (state) => state.khoaHocSlicer
  );

  const [arrKhoaHoc, setArrKhoaHoc] = useState([]);

  //Pagnation
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setpageSize] = useState(8);
  const [content, setContent] = useState([]);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    learningService
      .getKhoaHocTheoDanhMuc(params.MaDanhMuc)
      .then((res) => {
        setCurrentPage(1);
        setArrKhoaHoc(res.data);
        getTenKhoaHoc();
        setContent(res.data.slice(0, 8));
        setTotal(res.data.length);
      })
      .catch((err) => {
        message.error(err.response.data);
        setTimeout(() => {
          navigate("/");
        }, 1000);
      });
  }, [params]);

  useEffect(() => {
    getTenKhoaHoc();
  }, [danhMucKhoaHoc]);

  const getTenKhoaHoc = () => {
    danhMucKhoaHoc.forEach((item) => {
      if (item.maDanhMuc == params.MaDanhMuc) {
        store.dispatch(setTenKhoaHoc(item.tenDanhMuc));
      }
    });
  };

  const handleRenderKhoaHoc = () => {
    return content?.map((item) => {
      return <VideoItem key={nanoid()} item={item} />;
    });
  };

  //Pagnation
  const onChange = (page) => {
    setCurrentPage(page);
  };

  useEffect(() => {
    setContent(
      arrKhoaHoc.slice((currentPage - 1) * pageSize, pageSize * currentPage)
    );
  }, [pageSize, currentPage, total]);

  return (
    <div className="flex flex-col items-start grow">
      <div className="w-full bg-gradient-to-r from-rose-700 to-orange-400">
        <div className="container mx-auto text-start py-5 px-3">
          <p className="font-bold text-xl sm:text-2xl md:text-3xl lg:text-4xl text-white">
            {tenKhoaHoc}
          </p>
        </div>
      </div>
      <div className="sm:container mx-auto mt-4 px-3">
        <h2 className="text-start text-2xl pb-5 font-semibold border-b border-gray-200">
          Các khóa học phổ biến
        </h2>
        <div className="flex flex-col-reverse md:flex-col mb-5">
          <div className="sm:mt-5">
            <Pagination
              current={currentPage}
              onChange={onChange}
              pageSize={pageSize}
              total={total}
            />
          </div>
          <div className="grid px-5 grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-5 my-5">
            {handleRenderKhoaHoc()}
          </div>
        </div>
      </div>
    </div>
  );
}
