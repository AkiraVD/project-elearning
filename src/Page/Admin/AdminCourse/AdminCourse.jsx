import { Input, message, Select, Table } from "antd";
import React, { useEffect, useState } from "react";
import { learningService } from "../../../services/learningService";
import { courseColumns } from "../../../Utilities/adminUtilities";
import { UilSearch } from "@iconscout/react-unicons";
import { adminService } from "../../../services/adminService";
import { NavLink } from "react-router-dom";
import CourseEditModal from "./CourseEditModal/CourseEditModal";
import CourseRegisterDrawer from "./CourseRegisterDrawer/CourseRegisterDrawer";

export default function AdminCourse() {
  const [danhMucKhoaHoc, setDanhMucKhoaHoc] = useState([]);
  const [coursesData, setCoursesData] = useState([]);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [editingData, setEditingData] = useState({});
  const [open, setOpen] = useState(false);

  const [registeredUser, setRegisteredUser] = useState([]);
  const [pendingUser, setPendingUser] = useState([]);
  const [approvedUser, setApprovedUser] = useState([]);
  const [selectedCourse, setSelectedCourse] = useState("");

  const fetchData = () => {
    learningService
      .getDanhSachKhoaHoc()
      .then((res) => {
        setCoursesData(addButtonsToData(res.data));
      })
      .catch((err) => {});
  };

  // fetch data khoa hoc
  useEffect(() => {
    fetchData();
  }, []);

  // Them danh muc khoa hoc
  useEffect(() => {
    learningService
      .getDanhMucKhoaHoc()
      .then((res) => {
        setDanhMucKhoaHoc(
          res.data.map((item) => {
            return (
              <Select.Option value={item.maDanhMuc}>
                {item.tenDanhMuc}
              </Select.Option>
            );
          })
        );
      })
      .catch((err) => {});
  }, []);

  const handleXoaKhoaHoc = (maKhoaHoc) => {
    adminService
      .deleteKhoaHoc(maKhoaHoc)
      .then((res) => {
        message.success("Xóa khóa học thành công");
        fetchData();
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  const handleSuaKhoaHoc = (data) => {
    setEditingData(data);
    setIsModalOpen(true);
  };

  const handleGhiDanhHocVien = (maKhoaHoc) => {
    setSelectedCourse(maKhoaHoc);
    adminService
      .postDanhSachNguoiDungChuaGhiDanh(maKhoaHoc)
      .then((res) => {
        setRegisteredUser(res.data);
      })
      .catch((err) => {});
    adminService
      .postDanhSachHocVienChoXetDuyet(maKhoaHoc)
      .then((res) => {
        setPendingUser(res.data);
      })
      .catch((err) => {});
    adminService
      .postDanhSachHocVienKhoaHoc(maKhoaHoc)
      .then((res) => {
        setApprovedUser(res.data);
      })
      .catch((err) => {});
    setOpen(true);
  };

  const addButtonsToData = (data) => {
    return data.map((item) => {
      return {
        ...item,
        action: (
          <div className="text-white font-semibold flex flex-col gap-1">
            <button
              onClick={() => {
                handleGhiDanhHocVien(item.maKhoaHoc);
                scrollToTop();
              }}
              className="bg-blue-500 rounded px-3 py-1"
            >
              Ghi danh
            </button>
            <button
              onClick={() => {
                handleSuaKhoaHoc(item);
              }}
              className="bg-yellow-500 rounded px-3 py-1"
            >
              Sửa
            </button>
            <button
              onClick={() => {
                handleXoaKhoaHoc(item.maKhoaHoc);
              }}
              className="bg-red-500 rounded px-3 py-1"
            >
              Xóa
            </button>
          </div>
        ),
      };
    });
  };

  const handleSearchCourse = (keyword) => {
    if (!keyword) {
      return fetchData();
    }
    adminService
      .getTimKiemKhoaHoc(keyword)
      .then((res) => {
        setCoursesData(addButtonsToData(res.data));
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  const onCloseDrawer = () => {
    setOpen(false);
  };

  const containerStyle = {
    position: "relative",
    overflow: "hidden",
    textAlign: "center",
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div style={containerStyle} className="grow bg-sky-100 p-3">
      <h2 className="text-2xl font-semibold">Danh sách khóa học</h2>
      <div className="my-3 flex w-full justify-between px-10">
        <NavLink to="/Admin/AddCourse">
          <button className="bg-green-500 rounded px-3 py-1 text-white font-semibold hover:bg-opacity-75 active:bg-opacity-50">
            + Thêm khóa học
          </button>
        </NavLink>
        <div className="w-[50%]">
          <Input.Search
            placeholder="Nhập khóa học cần tìm"
            onSearch={handleSearchCourse}
            enterButton={<UilSearch />}
            className="bg-blue-500 rounded-lg"
          />
        </div>
      </div>
      <div>
        <Table columns={courseColumns} dataSource={coursesData} />
        <CourseEditModal
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          courseData={editingData}
          fetchData={fetchData}
          danhMucKhoaHoc={danhMucKhoaHoc}
        />

        <CourseRegisterDrawer
          open={open}
          onClose={onCloseDrawer}
          registeredUser={registeredUser}
          pendingUser={pendingUser}
          approvedUser={approvedUser}
          khoaHoc={selectedCourse}
          handleGhiDanhHocVien={handleGhiDanhHocVien}
          setPendingUser={setPendingUser}
          setApprovedUser={setApprovedUser}
        />
      </div>
    </div>
  );
}
