import { Drawer, message, Select, Table } from "antd";
import React, { useState } from "react";
import { adminService } from "../../../../services/adminService";
import { usersColumns } from "../../../../Utilities/adminUtilities";

export default function CourseRegisterDrawer({
  open,
  onClose,
  registeredUser,
  pendingUser,
  approvedUser,
  khoaHoc,
  handleGhiDanhHocVien,
  setPendingUser,
  setApprovedUser,
}) {
  const [user, setUser] = useState("");
  const onChange = (value) => {
    setUser(value);
  };
  const onSearch = (value) => {};

  const handleGhiDanh = (taiKhoan) => {
    adminService
      .postGhiDanhKhoaHoc(taiKhoan, khoaHoc)
      .then((res) => {
        message.success(res.data);
        handleGhiDanhHocVien(khoaHoc);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  const handleHuyGhiDanh = (taiKhoan, setArr, arr) => {
    adminService
      .postHuyGhiDanh(taiKhoan, khoaHoc)
      .then((res) => {
        message.success(res.data);
        setArr(arr);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  return (
    <Drawer
      title={<h3 className="text-2xl text-sky-600">Ghi danh khóa học</h3>}
      placement="left"
      closable={true}
      onClose={onClose}
      open={open}
      getContainer={false}
      height="100%"
      width={800}
    >
      <div>
        <div>
          <h4 className="text-xl mb-3 text-start">
            <span className="font-semibold">Khóa học:</span> {khoaHoc}
          </h4>
        </div>
        <h4 className="text-xl mb-3 text-start font-semibold">Chọn học viên</h4>
        <div className="flex w-full justify-start gap-5">
          <Select
            showSearch
            placeholder="Chọn học viên"
            optionFilterProp="children"
            onChange={onChange}
            onSearch={onSearch}
            className="min-w-[400px]"
            filterOption={(input, option) =>
              (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
            }
            options={registeredUser.map((item) => {
              return {
                label: item.hoTen,
                value: item.taiKhoan,
              };
            })}
          />
          <button
            onClick={() => {
              handleGhiDanh(user);
            }}
            className="bg-green-500 text-white font-semibold rounded px-3 py-1"
          >
            Ghi danh
          </button>
        </div>
        <div>
          <h4 className="text-xl my-3 text-start font-semibold">
            Khóa học chờ xác thực
          </h4>
          <Table
            scroll={{ y: 189 }}
            columns={usersColumns}
            dataSource={pendingUser.map((item) => {
              return {
                ...item,
                action: (
                  <>
                    <button
                      className="bg-blue-500 text-white px-3 py-1 rounded mr-3"
                      onClick={() => {
                        handleGhiDanh(item.taiKhoan);
                        setPendingUser(
                          pendingUser.filter((altItem) => {
                            return altItem.taiKhoan !== item.taiKhoan;
                          })
                        );
                      }}
                    >
                      Xác nhận
                    </button>
                    <button
                      className="bg-red-500 text-white px-3 py-1 rounded"
                      onClick={() => {
                        handleHuyGhiDanh(
                          item.taiKhoan,
                          setPendingUser,
                          pendingUser.filter((altItem) => {
                            return altItem.taiKhoan !== item.taiKhoan;
                          })
                        );
                      }}
                    >
                      Hủy
                    </button>
                  </>
                ),
              };
            })}
          />
        </div>
        <div>
          <h4 className="text-xl my-3 text-start font-semibold">
            Khóa học đã ghi danh
          </h4>
          <Table
            scroll={{ y: 189 }}
            columns={usersColumns}
            dataSource={approvedUser.map((item) => {
              return {
                ...item,
                action: (
                  <>
                    <button
                      className="bg-red-500 text-white px-3 py-1 rounded"
                      onClick={() => {
                        handleHuyGhiDanh(
                          item.taiKhoan,
                          setApprovedUser,
                          approvedUser.filter((altItem) => {
                            return altItem.taiKhoan !== item.taiKhoan;
                          })
                        );
                      }}
                    >
                      Hủy
                    </button>
                  </>
                ),
              };
            })}
          />
        </div>
      </div>
    </Drawer>
  );
}
