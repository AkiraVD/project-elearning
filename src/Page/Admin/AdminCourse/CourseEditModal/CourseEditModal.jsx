import { Button, Form, Input, message, Modal, Select } from "antd";
import React, { useEffect, useState } from "react";
import { adminService } from "../../../../services/adminService";
import { toKebabCase } from "../../../../Utilities/adminUtilities";

export default function CourseEditModal({
  isModalOpen,
  setIsModalOpen,
  courseData,
  fetchData,
  danhMucKhoaHoc,
}) {
  const [image, setImage] = useState(null);

  const [form] = Form.useForm();
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    form.setFieldsValue(courseData);
    form.setFieldValue("taiKhoanNguoiTao", courseData.nguoiTao?.taiKhoan);
    form.setFieldValue(
      "maDanhMucKhoaHoc",
      courseData.danhMucKhoaHoc?.maDanhMucKhoahoc
    );
    setImage(null);
  }, [courseData]);

  const UploadImage = (tenKhoaHoc) => {
    let frm = new FormData();
    frm.append("file", image);
    frm.append("tenKhoaHoc", tenKhoaHoc);
    adminService
      .postUploadHinhAnhKhoaHoc(frm)
      .then((res) => {
        message.success("Cập nhật hình ảnh thành công");
        fetchData();
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  const onFinish = (values) => {
    let biDanh = toKebabCase(values.tenKhoaHoc);
    let pushData = {
      ...values,
      hinhAnh: image ? image.name : courseData.hinhAnh,
      maNhom: courseData.maNhom,
      biDanh: biDanh,
    };
    adminService
      .putCapNhatKhoaHoc(pushData)
      .then((res) => {
        message.success("Cập nhật khóa học thành công");
        if (image) {
          UploadImage(biDanh);
        }
        fetchData();
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  return (
    <div>
      <Modal
        title={
          <p className="text-xl text-blue-500 pb-3">Sửa thông tin khóa học</p>
        }
        open={isModalOpen}
        onCancel={handleCancel}
        okText="Cập nhật"
        okButtonProps={{
          className: "bg-blue-500",
          type: "submit",
        }}
        width={1000}
        footer={[]}
      >
        <Form form={form} onFinish={onFinish} className="text-start">
          <Form.Item
            label={<h3 className="font-semibold text-xl">Mã khóa học</h3>}
            name="maKhoaHoc"
          >
            <p className="font-semibold text-xl">{courseData.maKhoaHoc}</p>
          </Form.Item>
          <div className="flex w-full">
            <div className="w-2/5">
              <Form.Item
                label={<h3 className="font-semibold">Người tạo</h3>}
                name="taiKhoanNguoiTao"
                labelCol={{ span: 9 }}
              >
                <p>{courseData.nguoiTao?.hoTen}</p>
              </Form.Item>
              <Form.Item
                label={<h3 className="font-semibold">Ngày tạo</h3>}
                name="ngayTao"
                labelCol={{ span: 9 }}
              >
                <p>{courseData.ngayTao}</p>
              </Form.Item>
              <Form.Item
                label={<h3 className="font-semibold">Tên khóa học</h3>}
                name="tenKhoaHoc"
                labelCol={{ span: 9 }}
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập tên khóa học!",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label={<h3 className="font-semibold">Danh mục khóa học</h3>}
                name="maDanhMucKhoaHoc"
                labelCol={{ span: 9 }}
              >
                <Select placeholder="Chọn danh mục khóa học">
                  {danhMucKhoaHoc}
                </Select>
              </Form.Item>
            </div>
            <div className="w-3/5">
              <Form.Item
                label={<h3 className="font-semibold">Lượt xem</h3>}
                name="luotXem"
                labelCol={{ span: 4 }}
                initialValue={0}
                rules={[
                  {
                    pattern: /^[0-9]*$/,
                    message: "Chỉ được nhập số!",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label={<h3 className="font-semibold">Đánh giá</h3>}
                name="danhGia"
                labelCol={{ span: 4 }}
                extra="Từ 0 đến 10"
                initialValue={0}
                rules={[
                  {
                    pattern: /^[0-9]*$/,
                    message: "Chỉ được nhập số!",
                  },
                ]}
              >
                <Input min={0} max={10} />
              </Form.Item>

              <Form.Item
                label={<h3 className="font-semibold">Mô tả</h3>}
                name="moTa"
                labelCol={{ span: 4 }}
                rules={[
                  {
                    required: true,
                    message: "Mời nhập mô tả khóa học!",
                  },
                ]}
              >
                <Input.TextArea showCount maxLength={100} />
              </Form.Item>
            </div>
          </div>

          <Form.Item
            label={<h3 className="font-semibold">Hình ảnh khóa học</h3>}
          >
            <div className="flex gap-2">
              <Input
                type="file"
                onChange={(event) => {
                  setImage(event.target.files[0]);
                }}
              />
              <Button
                onClick={() => {
                  setImage(null);
                }}
              >
                Reset
              </Button>
            </div>
            <div className="aspect-video bg-sky-200 h-[200px] overflow-hidden mx-auto mt-2">
              <img
                alt=""
                className="object-contain w-full h-full"
                src={image ? URL.createObjectURL(image) : courseData.hinhAnh}
              />
            </div>
          </Form.Item>

          <div className="text-end">
            <Form.Item className="inline-block">
              <Button
                type="primary"
                className="bg-blue-500 mr-3"
                htmlType="submit"
              >
                Cập nhật
              </Button>
            </Form.Item>
            <Form.Item className="inline-block">
              <Button key="back" onClick={handleCancel}>
                Hủy
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
    </div>
  );
}
