import { Form, Input, message, Select } from "antd";
import React from "react";
import { adminService } from "../../../services/adminService";
import { group } from "../../../Utilities/adminUtilities";

export default function AdminAddUser() {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    let pushData = { ...values, maNhom: group };
    adminService
      .postThemNguoiDung(pushData)
      .then((res) => {
        message.success("Thêm tài khoản thành công");
        form.resetFields();
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  return (
    <div className="bg-sky-100 grow">
      <div className="w-[720px] mx-auto">
        <h2 className="text-2xl font-semibold p-3">Thêm người dùng mới</h2>
        <br />
        <Form
          form={form}
          onFinish={onFinish}
          labelCol={{ span: 5 }}
          className="text-start"
        >
          <Form.Item
            label={<h3 className="font-semibold">Tên tài khoản</h3>}
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên tài khoản người dùng!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={<h3 className="font-semibold">Loại người dùng</h3>}
            name="maLoaiNguoiDung"
            rules={[
              {
                required: true,
                message: "Vui lòng chọn nhóm người dùng!",
              },
            ]}
          >
            <Select placeholder="Chọn loại người dùng">
              <Select.Option value="HV">Học viên</Select.Option>
              <Select.Option value="GV">Giáo viên</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="hoTen"
            label={<h3 className="font-semibold">Họ và Tên</h3>}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập họ tên người dùng!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="soDt"
            label={<h3 className="font-semibold">Số điện thoại</h3>}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập số điện thoại!",
              },
            ]}
          >
            <Input type="phone" />
          </Form.Item>
          <Form.Item
            name="email"
            label={<h3 className="font-semibold">Email</h3>}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập email!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="matKhau"
            label={<h3 className="font-semibold">Mật khẩu</h3>}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu!",
              },
            ]}
          >
            <Input.Password type="password" />
          </Form.Item>
          <div className="flex justify-between items-start">
            <button
              className="bg-yellow-500 text-white px-3 py-1 rounded hover:bg-opacity-75 active:bg-opacity-50"
              onClick={() => {
                window.history.back();
              }}
            >
              {"<--"} Quay lại
            </button>
            <div className="text-end">
              <Form.Item className="inline-block">
                <button
                  className="bg-blue-500 mr-3 text-white px-3 py-1 rounded hover:bg-opacity-75 active:bg-opacity-50"
                  htmlType="submit"
                >
                  Thêm người dùng
                </button>
              </Form.Item>
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
}
