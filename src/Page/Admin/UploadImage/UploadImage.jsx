import React, { useState } from "react";
import { Button, Input, message } from "antd";
import { adminService } from "../../../services/adminService";

export default function UploadImage({ khoaHoc }) {
  const [selectedImage, setSelectedImage] = useState(null);

  const handleUploadHinhAnh = () => {
    // 3 đoạn code này là data để post
    let frm = new FormData();
    frm.append("file", selectedImage);
    frm.append("tenKhoaHoc", khoaHoc.tenKhoaHoc);
    //
    adminService
      .postThemHinhAnhKhoaHoc(frm)
      .then((res) => {
        message.success("Upload Thanh Cong");
      })
      .catch((err) => {
        message.error("Upload That bai");
      });
  };

  return (
    <div className="flex flex-col justify-center items-center gap-2">
      <div>
        <div className="aspect-video bg-sky-200 h-[200px] overflow-hidden">
          <img
            alt=""
            className="object-contain w-full h-full"
            src={selectedImage ? URL.createObjectURL(selectedImage) : ""}
          />
        </div>
      </div>
      <br />

      <br />
      <div className="flex gap-2">
        <Input
          className="w-[400px]"
          type="file" // dùng để chọn hình up lên web
          name="myImage"
          onChange={(event) => {
            setSelectedImage(event.target.files[0]);
          }}
        />
        <Button // xóa hình đã up
          onClick={() => {
            setSelectedImage(null);
          }}
        >
          Remove
        </Button>
      </div>
      <Button
        type="danger"
        className="bg-red-500"
        onClick={handleUploadHinhAnh}
      >
        Upload
      </Button>
    </div>
  );
}
