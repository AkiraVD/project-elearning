import { Drawer, message, Select, Table } from "antd";
import React, { useState } from "react";
import { adminService } from "../../../../services/adminService";
import { coursesColumns } from "../../../../Utilities/adminUtilities";

export default function UserRegisterDrawer({
  open,
  onClose,
  registeredCourses,
  pendingCourses,
  approvedCourses,
  taiKhoan,
  handleGhiDanhKhoaHoc,
  setPendingCourses,
  setApprovedCourses,
}) {
  const [course, setCourse] = useState("");
  // Ghi danh funtion
  const onChange = (value) => {
    setCourse(value);
  };
  const onSearch = (value) => {};

  const handleGhiDanh = (maKhoaHoc) => {
    adminService
      .postGhiDanhKhoaHoc(taiKhoan, maKhoaHoc)
      .then((res) => {
        message.success(res.data);
        handleGhiDanhKhoaHoc(taiKhoan);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  const handleHuyGhiDanh = (maKhoaHoc, setArr, arr) => {
    adminService
      .postHuyGhiDanh(taiKhoan, maKhoaHoc)
      .then((res) => {
        message.success(res.data);
        setArr(arr);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  return (
    <Drawer
      title={<h3 className="text-2xl text-sky-600">Ghi danh khóa học</h3>}
      placement="left"
      closable={true}
      onClose={onClose}
      open={open}
      getContainer={false}
      height="100%"
      width={800}
    >
      <div>
        <div>
          <h4 className="text-xl mb-3 text-start">
            <span className="font-semibold">Tài khoản:</span> {taiKhoan}
          </h4>
        </div>
        <h4 className="text-xl mb-3 text-start font-semibold">Chọn khóa học</h4>
        <div className="flex w-full justify-start gap-5">
          <Select
            showSearch
            placeholder="Chọn khóa học"
            optionFilterProp="children"
            onChange={onChange}
            onSearch={onSearch}
            className="min-w-[400px]"
            filterOption={(input, option) =>
              (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
            }
            options={registeredCourses.map((item) => {
              return {
                label: item.tenKhoaHoc,
                value: item.maKhoaHoc,
              };
            })}
          />
          <button
            onClick={() => {
              handleGhiDanh(course);
            }}
            className="bg-green-500 text-white font-semibold rounded px-3 py-1"
          >
            Ghi danh
          </button>
        </div>
        <div>
          <h4 className="text-xl my-3 text-start font-semibold">
            Khóa học chờ xác thực
          </h4>
          <Table
            scroll={{ y: 189 }}
            columns={coursesColumns}
            dataSource={pendingCourses.map((item) => {
              return {
                ...item,
                action: (
                  <>
                    <button
                      className="bg-blue-500 text-white px-3 py-1 rounded mr-3"
                      onClick={() => {
                        handleGhiDanh(item.maKhoaHoc);
                        setPendingCourses(
                          pendingCourses.filter((altItem) => {
                            return altItem.maKhoaHoc !== item.maKhoaHoc;
                          })
                        );
                      }}
                    >
                      Xác nhận
                    </button>
                    <button
                      className="bg-red-500 text-white px-3 py-1 rounded"
                      onClick={() => {
                        handleHuyGhiDanh(
                          item.maKhoaHoc,
                          setPendingCourses,
                          pendingCourses.filter((altItem) => {
                            return altItem.maKhoaHoc !== item.maKhoaHoc;
                          })
                        );
                      }}
                    >
                      Hủy
                    </button>
                  </>
                ),
              };
            })}
          />
        </div>
        <div>
          <h4 className="text-xl my-3 text-start font-semibold">
            Khóa học đã ghi danh
          </h4>
          <Table
            scroll={{ y: 189 }}
            columns={coursesColumns}
            dataSource={approvedCourses.map((item) => {
              return {
                ...item,
                action: (
                  <>
                    <button
                      className="bg-red-500 text-white px-3 py-1 rounded"
                      onClick={() => {
                        handleHuyGhiDanh(
                          item.maKhoaHoc,
                          setApprovedCourses,
                          approvedCourses.filter((altItem) => {
                            return altItem.maKhoaHoc !== item.maKhoaHoc;
                          })
                        );
                      }}
                    >
                      Hủy
                    </button>
                  </>
                ),
              };
            })}
          />
        </div>
      </div>
    </Drawer>
  );
}
