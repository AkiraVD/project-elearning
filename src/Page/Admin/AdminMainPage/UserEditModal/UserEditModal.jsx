import { Button, Form, Input, message, Modal, Select } from "antd";
import React, { useEffect } from "react";
import { adminService } from "../../../../services/adminService";

export default function UserEditModal({
  isModalOpen,
  setIsModalOpen,
  userData,
  fetchData,
}) {
  const [form] = Form.useForm();
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    form.setFieldsValue(userData);
  }, [userData]);

  const onFinish = (values) => {
    adminService
      .putCapNhatNguoiDung(values)
      .then((res) => {
        message.success("Cập nhật thành công");
        fetchData();
        setIsModalOpen(false);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };
  return (
    <Modal
      title={
        <p className="text-xl text-blue-500 pb-3">Sửa thông tin người dùng</p>
      }
      open={isModalOpen}
      onCancel={handleCancel}
      okText="Cập nhật"
      okButtonProps={{
        className: "bg-blue-500",
        type: "submit",
      }}
      width={1000}
      footer={[]}
    >
      <Form
        form={form}
        onFinish={onFinish}
        labelCol={{ span: 8 }}
        className="text-start"
      >
        <div className="flex">
          <div className="w-1/2">
            <Form.Item
              label={<h3 className="font-semibold">Tên tài khoản</h3>}
              name="taiKhoan"
            >
              <p>{userData.taiKhoan}</p>
            </Form.Item>
            <Form.Item
              label={<h3 className="font-semibold">Loại người dùng</h3>}
              name="maLoaiNguoiDung"
            >
              <Select defaultValue={userData.maLoaiNguoiDung}>
                <Select.Option value="HV">Học viên</Select.Option>
                <Select.Option value="GV">Giáo viên</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item
              name="hoTen"
              label={<h3 className="font-semibold">Họ và Tên</h3>}
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập họ tên người dùng!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </div>
          <div className="w-1/2">
            <Form.Item
              name="soDt"
              label={<h3 className="font-semibold">Số điện thoại</h3>}
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập số điện thoại!",
                },
              ]}
            >
              <Input type="phone" />
            </Form.Item>
            <Form.Item
              name="email"
              label={<h3 className="font-semibold">Email</h3>}
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập email!",
                },
              ]}
            >
              <Input type="email" />
            </Form.Item>
            <Form.Item
              name="matKhau"
              label={<h3 className="font-semibold">Mật khẩu</h3>}
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu!",
                },
              ]}
            >
              <Input.Password type="password" />
            </Form.Item>
          </div>
        </div>
        <div className="text-end">
          <Form.Item className="inline-block">
            <Button
              type="primary"
              className="bg-blue-500 mr-3"
              htmlType="submit"
            >
              Cập nhật
            </Button>
          </Form.Item>
          <Form.Item className="inline-block">
            <Button key="back" onClick={handleCancel}>
              Hủy
            </Button>
          </Form.Item>
        </div>
      </Form>
    </Modal>
  );
}
