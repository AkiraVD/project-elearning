import React, { useEffect, useState } from "react";
import { adminService } from "../../../services/adminService";
import { Table, Input, message } from "antd";
import { userColumns } from "../../../Utilities/adminUtilities";
import { UilSearch } from "@iconscout/react-unicons";
import UserEditModal from "./UserEditModal/UserEditModal";
import UserRegisterDrawer from "./UserRegisterDrawer/UserRegisterDrawer";
import { NavLink } from "react-router-dom";

export default function AdminMainPage() {
  const [usersData, setUsersData] = useState([]);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [editingData, setEditingData] = useState({});

  const [open, setOpen] = useState(false);
  const [registeredCourses, setRegisteredCourses] = useState([]);
  const [pendingCourses, setPendingCourses] = useState([]);
  const [approvedCourses, setApprovedCourses] = useState([]);
  const [selectedUser, setSelectedUser] = useState("");

  const fetchData = () => {
    adminService
      .getDanhSachNguoiDung()
      .then((res) => {
        setUsersData(addButtonsToData(res.data));
      })
      .catch((err) => {});
  };

  const addButtonsToData = (data) => {
    return data.map((item) => {
      return {
        ...item,
        action: (
          <div className="text-white font-semibold">
            <button
              onClick={() => {
                handleGhiDanhKhoaHoc(item.taiKhoan);
              }}
              className="bg-blue-500 rounded px-3 py-1"
            >
              Ghi danh
            </button>
            <button
              onClick={() => {
                handleSuaNguoiDung(item);
              }}
              className="bg-yellow-500 rounded px-3 py-1 mx-2"
            >
              Sửa
            </button>
            <button
              onClick={() => {
                handleXoaNguoiDung(item.taiKhoan);
              }}
              className="bg-red-500 rounded px-3 py-1"
            >
              Xóa
            </button>
          </div>
        ),
      };
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  // Button funtions
  const handleXoaNguoiDung = (taiKhoan) => {
    adminService
      .deleteNguoiDung(taiKhoan)
      .then((res) => {
        message.success(taiKhoan + " " + res.data);
        fetchData();
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  const handleSuaNguoiDung = (data) => {
    setEditingData(data);
    setIsModalOpen(true);
  };

  const handleGhiDanhKhoaHoc = (taiKhoan) => {
    setSelectedUser(taiKhoan);
    adminService
      .postDanhSachKhoaHocChuaGhiDanh(taiKhoan)
      .then((res) => {
        setRegisteredCourses(res.data);
      })
      .catch((err) => {});
    adminService
      .postDanhSachKhoaHocChoXetDuyet(taiKhoan)
      .then((res) => {
        setPendingCourses(res.data);
      })
      .catch((err) => {});
    adminService
      .postDanhSachKhoaHocDaXetDuyet(taiKhoan)
      .then((res) => {
        setApprovedCourses(res.data);
      })
      .catch((err) => {});
    setOpen(true);
  };

  const handleSearchUser = (keyword) => {
    if (!keyword) {
      return fetchData();
    }
    adminService
      .getTimKiemNguoiDung(keyword)
      .then((res) => {
        setUsersData(addButtonsToData(res.data));
      })
      .catch((err) => {});
  };

  const onCloseDrawer = () => {
    setOpen(false);
  };

  const containerStyle = {
    position: "relative",
    overflow: "hidden",
    textAlign: "center",
  };

  return (
    <div style={containerStyle} className="grow bg-sky-100 p-3">
      <h2 className="text-2xl font-semibold">Danh sách người dùng</h2>
      <div className="my-3 flex w-full justify-between px-10">
        <NavLink to="/Admin/AddUser">
          <button className="bg-green-500 rounded px-3 py-1 text-white font-semibold hover:bg-opacity-75 active:bg-opacity-50">
            + Thêm người dùng
          </button>
        </NavLink>
        <div className="w-[50%]">
          <Input.Search
            placeholder="Nhập tài khoản cần tìm"
            onSearch={handleSearchUser}
            enterButton={<UilSearch />}
            className="bg-blue-500 rounded-lg"
          />
        </div>
      </div>
      <div>
        <Table columns={userColumns} dataSource={usersData} />
        <UserEditModal
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          userData={editingData}
          fetchData={fetchData}
        />

        <UserRegisterDrawer
          open={open}
          onClose={onCloseDrawer}
          registeredCourses={registeredCourses}
          pendingCourses={pendingCourses}
          approvedCourses={approvedCourses}
          taiKhoan={selectedUser}
          handleGhiDanhKhoaHoc={handleGhiDanhKhoaHoc}
          setPendingCourses={setPendingCourses}
          setApprovedCourses={setApprovedCourses}
        />
      </div>
    </div>
  );
}
