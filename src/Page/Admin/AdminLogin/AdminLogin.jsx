import React, { useEffect } from "react";
import { Button, Form, Input, message } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { ADMIN_ACC, ADMIN_PASS } from "../adminAccount";
import { store } from "../../../app/store";
import { useSelector } from "react-redux";
import { setAdminLoginStatus } from "../../../redux-toolkit/slice/adminSlicer";

export default function AdminLogin() {
  let navigate = useNavigate();
  let adminStatus = useSelector((state) => state.adminSlicer?.isAdminLogin);
  let maLoaiNguoiDung = useSelector(
    (state) => state.userSlicer.userInfo?.maLoaiNguoiDung
  );
  useEffect(() => {
    if (maLoaiNguoiDung == "GV") {
      if (adminStatus) {
        navigate("/Admin/Main");
      }
    } else {
      message.error("Tài khoản không đủ quyền truy cập");
      setTimeout(() => {
        message.warning("Quay về trang chủ");
      }, 1500);
      setTimeout(() => {
        navigate("/");
      }, 1500);
    }
  }, [adminStatus]);

  const onFinish = (values) => {
    if (values.username !== ADMIN_ACC || values.password !== ADMIN_PASS) {
      message.error("Sai tài khoản hoặc mật khẩu");
      return false;
    } else {
      message.success("Đăng nhập thành công");
      store.dispatch(setAdminLoginStatus());
      setTimeout(() => {
        navigate("/Admin/Main");
      }, 1000);
    }
  };

  return (
    <div className="flex justify-center h-screen items-center bg-sky-100">
      <div className="bg-white p-5 rounded-xl">
        <Form
          name="basic"
          labelCol={{
            span: 11,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item label="Tài khoản admin" name="username">
            <Input />
          </Form.Item>

          <Form.Item label="Mật khẩu" name="password">
            <Input.Password />
          </Form.Item>
          <Form.Item
            wrapperCol={{
              span: 100,
            }}
          >
            <NavLink to="/">
              <Button type="warning" className="bg-yellow-500 mx-2">
                Quay về trang chủ
              </Button>
            </NavLink>
            <Button
              type="primary"
              htmlType="submit"
              className="bg-blue-500  mx-2"
            >
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
