import { Button, Form, Input, message, Select } from "antd";
import moment from "moment/moment";
import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { adminService } from "../../../services/adminService";
import { learningService } from "../../../services/learningService";
import { toKebabCase } from "../../../Utilities/adminUtilities";
export default function AdminAddCourse() {
  const [danhMucKhoaHoc, setDanhMucKhoaHoc] = useState([]);
  const [image, setImage] = useState(null);
  let { taiKhoan } = useSelector((state) => state.userSlicer.userInfo);

  const UploadImage = (tenKhoaHoc) => {
    let frm = new FormData();
    frm.append("file", image);
    frm.append("tenKhoaHoc", tenKhoaHoc);
    adminService
      .postUploadHinhAnhKhoaHoc(frm)
      .then((res) => {
        message.success("Upload hình ảnh thành công");
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  const [form] = Form.useForm();
  const onFinish = (values) => {
    let biDanh = toKebabCase(values.tenKhoaHoc);
    let pushData = {
      ...values,
      maNhom: "GP01",
      bidanh: biDanh,
      taiKhoanNguoiTao: taiKhoan,
      ngayTao: moment(today).format("DD/MM/YYYY"),
      hinhAnh: image.name,
    };
    adminService
      .postThemKhoaHoc(pushData)
      .then((res) => {
        message.success("Thêm khóa học thành công");
        UploadImage(biDanh);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  useEffect(() => {
    learningService
      .getDanhMucKhoaHoc()
      .then((res) => {
        setDanhMucKhoaHoc(
          res.data.map((item) => {
            return (
              <Select.Option value={item.maDanhMuc}>
                {item.tenDanhMuc}
              </Select.Option>
            );
          })
        );
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  }, []);

  const today = new Date().toLocaleDateString();

  return (
    <div className="bg-sky-100 grow">
      <div className="w-[720px] mx-auto">
        <h2 className="text-2xl font-semibold p-3">Thêm khóa học mới</h2>
        <br />

        <Form
          form={form}
          onFinish={onFinish}
          labelCol={{ span: 5 }}
          className="text-start"
        >
          <Form.Item
            label={<h3 className="font-semibold">Ngày tạo</h3>}
            name="ngayTao"
          >
            <p>{moment(today).format("DD/MM/YYYY")}</p>
          </Form.Item>

          <Form.Item
            label={<h3 className="font-semibold">Mã khóa học</h3>}
            name="maKhoaHoc"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mã khóa học!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label={<h3 className="font-semibold">Tên khóa học</h3>}
            name="tenKhoaHoc"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên khóa học!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label={<h3 className="font-semibold">Danh mục khóa học</h3>}
            name="maDanhMucKhoaHoc"
            rules={[
              {
                required: true,
                message: "Vui lòng chọn danh mục khóa học!",
              },
            ]}
          >
            <Select placeholder="Chọn danh mục khóa học">
              {danhMucKhoaHoc}
            </Select>
          </Form.Item>

          <Form.Item
            label={<h3 className="font-semibold">Lượt xem</h3>}
            name="luotXem"
            initialValue={0}
            rules={[
              {
                pattern: /^[0-9]*$/,
                message: "Chỉ được nhập số!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label={<h3 className="font-semibold">Đánh giá</h3>}
            name="danhGia"
            extra="Từ 0 đến 10"
            initialValue={0}
            rules={[
              {
                pattern: /^[0-9]*$/,
                message: "Chỉ được nhập số!",
              },
            ]}
          >
            <Input min={0} max={10} />
          </Form.Item>

          <Form.Item
            label={<h3 className="font-semibold">Mô tả</h3>}
            name="moTa"
            rules={[
              {
                required: true,
                message: "Mời nhập mô tả khóa học!",
              },
            ]}
          >
            <Input.TextArea showCount maxLength={100} />
          </Form.Item>

          <div className="aspect-video bg-sky-200 h-[200px] overflow-hidden mx-auto my-2">
            <img
              alt=""
              className="object-contain w-full h-full"
              src={image ? URL.createObjectURL(image) : ""}
            />
          </div>

          <Form.Item
            label={<h3 className="font-semibold">Hình ảnh khóa học</h3>}
          >
            <div className="flex gap-2">
              <Input
                type="file"
                onChange={(event) => {
                  setImage(event.target.files[0]);
                }}
              />
              <Button
                onClick={() => {
                  setImage(null);
                }}
              >
                Remove
              </Button>
            </div>
          </Form.Item>

          <div className="flex justify-between items-start">
            <button
              className="bg-yellow-500 text-white px-3 py-1 rounded hover:bg-opacity-75 active:bg-opacity-50"
              onClick={() => {
                window.history.back();
              }}
            >
              {"<--"} Quay lại
            </button>
            <div className="text-end">
              <Form.Item className="inline-block">
                <button
                  className="bg-blue-500 mr-3 text-white px-3 py-1 rounded hover:bg-opacity-75 active:bg-opacity-50"
                  htmlType="submit"
                >
                  Thêm khóa học
                </button>
              </Form.Item>
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
}
