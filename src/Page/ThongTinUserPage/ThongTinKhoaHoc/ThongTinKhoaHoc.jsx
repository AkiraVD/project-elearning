import { message, Pagination } from "antd";
import Search from "antd/es/transfer/search";
import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
import { learningService } from "../../../services/learningService";
import ItemKhoaHoc from "./ItemKhoaHoc/ItemKhoaHoc";

export default function ThongTinKhoaHoc({ khoaHocData, setKhoaHocData }) {
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setpageSize] = useState(4);
  const [content, setContent] = useState([]);

  const [total, setTotal] = useState(0);
  const [dataArr, setDataArr] = useState([]);

  useEffect(() => {
    setData(khoaHocData);
  }, [khoaHocData]);

  const setData = (arr) => {
    setDataArr(arr);
    setContent(arr.slice(0, 4));
    setTotal(arr.length);
  };

  useEffect(() => {
    setContent(
      dataArr.slice((currentPage - 1) * pageSize, pageSize * currentPage)
    );
  }, [pageSize, currentPage, dataArr]);

  const onChange = (page) => {
    setCurrentPage(page);
  };

  const onSearch = (e) => {
    let keyword = `${e.target.value}`;
    let regex = new RegExp(keyword, "gmi");
    let newArr = khoaHocData.filter((item) => {
      return (
        regex.test(item.tenKhoaHoc) ||
        regex.test(item.biDanh) ||
        regex.test(item.maKhoaHoc)
      );
    });
    setCurrentPage(1);
    setData(newArr);
  };

  const handleHuyGhiDanh = (taiKhoan, maKhoaHoc) => {
    learningService
      .postHuyGhiDanh({ taiKhoan: taiKhoan, maKhoaHoc: maKhoaHoc })
      .then((res) => {
        message.success("Đã hủy đăng kí khóa " + maKhoaHoc);
        setKhoaHocData(
          khoaHocData.filter((item) => {
            return item.maKhoaHoc !== maKhoaHoc;
          })
        );
        if ((total - 1) % 4 == 0) {
          setCurrentPage(currentPage - 1);
        }
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  const renderThongTinKhoaHoc = () => {
    if (content.length !== 0) {
      return content.map((item) => {
        return (
          <ItemKhoaHoc
            key={nanoid()}
            item={item}
            handleHuyGhiDanh={handleHuyGhiDanh}
          />
        );
      });
    } else {
      return (
        <p className="text-lg text-start text-gray-300">
          Chưa có khóa học mới đăng ký
        </p>
      );
    }
  };

  return (
    <>
      <div className="flex flex-col sm:flex-row gap-3 justify-between mx-5">
        <div className="">
          <Search
            placeholder="Tìm kiếm"
            onChange={onSearch}
            enterButton="Search"
          />
        </div>
      </div>
      <div className="mb-5">
        <div className="grid px-5 lg:grid-cols-2 gap-5 my-5">
          {renderThongTinKhoaHoc()}
        </div>
        <div className="sm:mt-5">
          <Pagination
            defaultCurrent={1}
            current={currentPage}
            onChange={onChange}
            pageSize={pageSize}
            total={total}
          />
        </div>
      </div>
    </>
  );
}
