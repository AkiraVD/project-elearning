import React from "react";
import { UisStar } from "@iconscout/react-unicons-solid";
import { UisStarHalfAlt } from "@iconscout/react-unicons-solid";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
export default function ItemKhoaHoc({ item, handleHuyGhiDanh }) {
  let user = useSelector((state) => state.userSlicer.userInfo);

  const renderStar = (point) => {
    let contents = [];
    for (let index = 0; index < Math.floor(point / 2); index++) {
      contents.push(
        <UisStar
          key={"icon" + index}
          size="20"
          className="text-yellow-400 inline-block"
        />
      );
    }
    if (point % 2 > 0) {
      contents.push(
        <UisStarHalfAlt
          key="icon half"
          size="20"
          className="text-yellow-400 inline-block"
        />
      );
    }
    return contents;
  };

  return (
    <div className="border flex border-gray-200 text-left text-lg hover:shadow-xl transition hover:-translate-y-1 duration-300">
      <NavLink
        to={`/ChiTiet/${item.maKhoaHoc}`}
        className="sm:w-1/3 aspect-video hidden sm:flex flex-col justify-center bg-black overflow-hidden text-white font-bold text-center text-4xl"
      >
        <img
          className="object-center w-full"
          src={item.hinhAnh}
          alt={item.tenKhoaHoc}
        />
      </NavLink>
      <div className="sm:w-2/3 py-3 pl-5 border-t border-gray-200 flex gap-10">
        <div>
          <NavLink
            to={`/ChiTiet/${item.maKhoaHoc}`}
            className="font-semibold"
            title={item.maKhoaHoc}
          >
            {item.tenKhoaHoc}
          </NavLink>
          <h4 className="text-sm text-gray-500">
            Ngày tạo: <span>{item.ngayTao}</span>
          </h4>
          <button
            onClick={() => {
              handleHuyGhiDanh(user.taiKhoan, item.maKhoaHoc);
            }}
            className="bg-red-500 text-white rounded px-4 py-2 mt-5"
          >
            Hủy
          </button>
        </div>
        <div>
          <h4 className="text-sm text-gray-500">{item.luotXem} lượt xem</h4>
          <span>{renderStar(4.5)}</span>
        </div>
      </div>
    </div>
  );
}
