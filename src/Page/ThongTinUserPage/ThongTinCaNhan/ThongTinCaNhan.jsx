import { Button, Form, Input, message, Tag } from "antd";
import React, { useState } from "react";
import { rules } from "../../../Utilities/validatedRules";
import { userService } from "../../../services/userService";
import { store } from "../../../app/store";
import { setUserInfo } from "../../../redux-toolkit/slice/userSlicer";
import { useEffect } from "react";

export default function ThongTinCaNhan({ userData }) {
  const [form] = Form.useForm();
  const [editing, setEditing] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    form.setFieldsValue({ ...userData, matKhau: "" });
  }, [userData]);

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const toggleEdit = () => {
    setEditing(!editing);
  };

  const onFinish = (values) => {
    userService
      .putCapNhatThongTin(values)
      .then((res) => {
        message.success("Cập nhật thông tin thành công");
        store.dispatch(setUserInfo({ ...userData, ...values }));
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  const onFinishFailed = (err) => {};

  const onCancel = () => {
    form.resetFields();
    toggleEdit();
  };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      labelCol={{ span: 9 }}
      className="text-start"
    >
      <div className="flex flex-col md:flex-row md:gap-10">
        <div className="md:w-1/2">
          <Form.Item
            label={<h3 className="font-semibold">Tên tài khoản</h3>}
            name="taiKhoan"
            initialValue={userData.taiKhoan}
            disabled
          >
            <p>{userData.taiKhoan}</p>
          </Form.Item>
          <Form.Item
            label={<h3 className="font-semibold">Mã nhóm</h3>}
            name="maNhom"
            initialValue={userData.maNhom}
            disabled
          >
            <p>{userData.maNhom}</p>
          </Form.Item>
          <Form.Item
            label={<h3 className="font-semibold">Loại người dùng</h3>}
            name="maLoaiNguoiDung"
            initialValue={userData.maLoaiNguoiDung}
            disabled
          >
            <p>
              {userData.maLoaiNguoiDung == "GV" ? (
                <Tag color="blue">Giáo Viên</Tag>
              ) : (
                <Tag color="green">Học Sinh</Tag>
              )}
            </p>
          </Form.Item>
          <Form.Item
            name="hoTen"
            label={<h3 className="font-semibold">Họ và Tên</h3>}
            initialValue={userData.hoTen}
            rules={rules.name}
          >
            {editing ? <Input /> : <p>{userData.hoTen}</p>}
          </Form.Item>
          <Form.Item
            name="soDT"
            label={<h3 className="font-semibold">Số điện thoại</h3>}
            initialValue={userData.soDT}
            rules={rules.phone}
          >
            {editing ? <Input type="phone" /> : <p>{userData.soDT}</p>}
          </Form.Item>
        </div>
        <div className="md:w-1/2">
          <Form.Item
            name="email"
            label={<h3 className="font-semibold">Email</h3>}
            initialValue={userData.email}
            rules={rules.email}
          >
            {editing ? <Input type="email" /> : <p>{userData.email}</p>}
          </Form.Item>
          <Form.Item
            name="matKhau"
            label={<h3 className="font-semibold">Mật khẩu</h3>}
            rules={rules.password}
          >
            {editing ? (
              <Input.Password
                type="password"
                eye
                visibilityToggle
                onClick={handleShowPassword}
                visible={showPassword}
              />
            ) : (
              <p>{userData.matKhau?.slice(0, 2) + "********"}</p>
            )}
          </Form.Item>
          {editing ? (
            <Form.Item
              name="xacNhanMatKhau"
              label={<h3 className="font-semibold">Nhập lại mật khẩu</h3>}
              dependencies={["matKhau"]}
              rules={rules.repassword}
            >
              <Input.Password
                type="password"
                eye
                visibilityToggle
                onClick={handleShowPassword}
                visible={showPassword}
              />
            </Form.Item>
          ) : null}
        </div>
      </div>
      {editing ? (
        <>
          <Form.Item className="text-center">
            <button
              type="submit"
              className="bg-yellow-400 hover:bg-yellow-300 active:bg-yellow-500 px-3 py-1 rounded duration-300"
            >
              Cập nhật
            </button>
          </Form.Item>
          <Form.Item className="text-center">
            <Button onClick={onCancel}>Cancel</Button>
          </Form.Item>
        </>
      ) : (
        <Form.Item className="">
          <Button onClick={toggleEdit}>Thay đổi thông tin cá nhân</Button>
        </Form.Item>
      )}
    </Form>
  );
}
