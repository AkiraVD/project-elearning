import { message, Tabs } from "antd";
import React, { useEffect, useState } from "react";
import user_banner from "../../assets/user-banner.bmp";
import { userService } from "../../services/userService";
import ThongTinCaNhan from "./ThongTinCaNhan/ThongTinCaNhan";
import ThongTinKhoaHoc from "./ThongTinKhoaHoc/ThongTinKhoaHoc";
export default function ThongTinUserPage() {
  const [userData, setUserData] = useState({});
  const [khoaHocData, setKhoaHocData] = useState([]);

  useEffect(() => {
    userService
      .postThongTinNguoiDung()
      .then((res) => {
        setUserData(res.data);
        setKhoaHocData(res.data.chiTietKhoaHocGhiDanh);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  }, []);

  const items = [
    {
      label: <p className="font-semibold text-lg">Thông tin cá nhân</p>,
      key: "1",
      children: (
        <div className="mx-5">
          <ThongTinCaNhan userData={userData} />
        </div>
      ),
    },
    {
      label: <p className="font-semibold text-lg">Khóa học đã ghi danh</p>,
      key: "2",
      children: (
        <ThongTinKhoaHoc
          khoaHocData={khoaHocData}
          setKhoaHocData={setKhoaHocData}
        />
      ),
    },
  ];
  const background_style = {
    backgroundImage: `url(${user_banner})`,
    backgroundSize: "100% 100%",
    backgroundPosition: "center",
  };
  return (
    <div className="flex flex-col justify-start grow">
      <div className="w-full bg-gradient-to-r from-orange-600 to-purple-500">
        <div className="container mx-auto text-start py-5 px-3">
          <p className="font-bold text-xl sm:text-2xl md:text-3xl lg:text-4xl text-white">
            Thông tin tài khoản
          </p>
        </div>
      </div>
      <div style={background_style} className="aspect-[12/1]"></div>
      <div className="container mx-auto grow">
        <Tabs
          tabBarStyle={{ marginLeft: "10px" }}
          defaultActiveKey="1"
          items={items}
        />
      </div>
    </div>
  );
}
