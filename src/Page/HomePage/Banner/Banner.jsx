import React from "react";
import Lottie from "lottie-react";
import banner_img from "../../../assets/banner-img.json";

export default function Banner() {
  return (
    <div className="bg-sky-100">
      <div className="container mx-auto flex flex-col-reverse md:flex-row">
        <div className="lg:w-1/2">
          <Lottie animationData={banner_img} loop={true} />{" "}
        </div>
        <div className="lg:w-1/2 flex flex-col justify-center py-5 lg:pr-5">
          <h2 className="text-sky-500 font-bold text-3xl md:text-5xl lg:text-7xl mb-5">
            KHỞI ĐẦU SỰ NGHIỆP CỦA BẠN
          </h2>
          <h3 className="font-bold text-xl md:text-2xl lg:text-3xl mb-5">
            Trở thành lập trình viên chuyên nghiệp tại Code Campus
          </h3>
          <div className="flex justify-center gap-5">
            <a href="#khoahocnew">
              <button className="bg-emerald-500 text-white px-4 py-2 rounded hover:opacity-70 active:opacity-30 duration-300">
                Xem khóa học
              </button>
            </a>
            <a href="#dangky">
              <button className="bg-sky-500 text-white px-4 py-2 rounded hover:opacity-70 active:opacity-30 duration-300">
                Tư vấn học
              </button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
