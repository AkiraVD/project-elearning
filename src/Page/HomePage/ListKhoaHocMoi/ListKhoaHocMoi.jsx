import { nanoid } from "nanoid";
import React from "react";
import VideoItem from "../../../Components/VideoItem/VideoItem";

export default function ListKhoaHocMoi({ arrKhoaHoc }) {
  const renderKhoaHocMoi = () => {
    return arrKhoaHoc?.slice(0, 8).map((item) => {
      return <VideoItem key={nanoid()} item={item} />;
    });
  };
  return (
    <>
      <h2
        id="khoahocnew"
        className="text-3xl sm:text-4xl p-5 font-semibold border-b border-gray-200"
      >
        Các khóa học mới nhất
      </h2>
      <div className="grid px-5 grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-5 my-5">
        {renderKhoaHocMoi()}
      </div>
    </>
  );
}
