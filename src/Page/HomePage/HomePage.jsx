import React, { useEffect, useState } from "react";
import { learningService } from "../../services/learningService";
import Banner from "./Banner/Banner";
import ListKhoaHocMoi from "./ListKhoaHocMoi/ListKhoaHocMoi";

export default function HomePage() {
  const [arrKhoaHoc, setArrKhoaHoc] = useState([]);

  useEffect(() => {
    learningService
      .getDanhSachKhoaHoc()
      .then((res) => {
        setArrKhoaHoc(res.data);
      })
      .catch((err) => {});
  }, []);

  return (
    <>
      <Banner />
      <div className="sm:container mx-auto">
        <ListKhoaHocMoi arrKhoaHoc={arrKhoaHoc} />
      </div>
    </>
  );
}
