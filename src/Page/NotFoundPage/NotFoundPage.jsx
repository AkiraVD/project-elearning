import React from "react";
import Lottie from "lottie-react";
import not_found_animate from "../../assets/not-found.json";

export default function NotFoundPage() {
  return (
    <div className="flex flex-col justify-center items-center grow py-10">
      <h4 className="text-6xl text-red-600 font-medium animate-bounce mb-10">
        404 PAGE NOT FOUND
      </h4>
      <Lottie animationData={not_found_animate} loop={true} />
    </div>
  );
}
