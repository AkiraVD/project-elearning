import "./App.css";
import Layout from "./Layout/Layout";
import HomePage from "./Page/HomePage/HomePage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Loading from "./Components/Loading/Loading";
import DanhMucKhoaHocPage from "./Page/DanhMucKhoaHocPage/DanhMucKhoaHocPage";
import ChiTietPage from "./Page/ChiTietPage/ChiTietPage";
import TimKiemPage from "./Page/TimKiemPage/TimKiemPage";
import DangNhapPage from "./Page/DangNhapPage/DangNhapPage";
import DangkyPage from "./Page/DangKyPage/DangkyPage";
import NotFoundPage from "./Page/NotFoundPage/NotFoundPage";
import ThongTinUserPage from "./Page/ThongTinUserPage/ThongTinUserPage";
import AdminLayout from "./Layout/AdminLayout/AdminLayout";
import AdminMainPage from "./Page/Admin/AdminMainPage/AdminMainPage";
import AdminLogin from "./Page/Admin/AdminLogin/AdminLogin";
import AdminCourse from "./Page/Admin/AdminCourse/AdminCourse";
import AdminAddUser from "./Page/Admin/AdminAddUser/AdminAddUser";
import AdminAddCourse from "./Page/Admin/AdminAddCourse/AdminAddCourse";

function App() {
  return (
    <div className="App">
      <Loading />
      <BrowserRouter>
        <Routes>
          <Route
            path="*"
            element={
              <Layout>
                <NotFoundPage />
              </Layout>
            }
          />
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route
            path="/ThongTinTaiKhoan"
            element={
              <Layout>
                <ThongTinUserPage />
              </Layout>
            }
          />
          <Route
            path="/DanhMucKhoaHoc/:MaDanhMuc"
            element={
              <Layout>
                <DanhMucKhoaHocPage />
              </Layout>
            }
          />
          <Route
            path="/ChiTiet/:maKhoaHoc"
            element={
              <Layout>
                <ChiTietPage />
              </Layout>
            }
          />
          <Route
            path="/TimKiemKhoaHoc/:tuKhoa"
            element={
              <Layout>
                <TimKiemPage />
              </Layout>
            }
          />
          <Route
            path="/DangNhap"
            element={
              <Layout>
                <DangNhapPage />
              </Layout>
            }
          />
          <Route
            path="/DangKy"
            element={
              <Layout>
                <DangkyPage />
              </Layout>
            }
          />
          {/* AdminPage */}
          <Route path="/Admin/Login" element={<AdminLogin />} />
          <Route
            path="/Admin/Main"
            element={
              <AdminLayout>
                <AdminMainPage />
              </AdminLayout>
            }
          />
          <Route
            path="/Admin/AddUser"
            element={
              <AdminLayout>
                <AdminAddUser />
              </AdminLayout>
            }
          />
          <Route
            path="/Admin/Course"
            element={
              <AdminLayout>
                <AdminCourse />
              </AdminLayout>
            }
          />
          <Route
            path="/Admin/AddCourse"
            element={
              <AdminLayout>
                <AdminAddCourse />
              </AdminLayout>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
